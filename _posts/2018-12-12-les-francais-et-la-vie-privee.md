---
layout: post
current: post
navigation: True
title: "Les Français·es et le respect de leur vie privée sur Internet"
date: 2018-12-12 12:00:00
tags: data-privacy
class: post-template
cover: assets/images/posts/2018-12-12-les-francais-et-la-vie-privee.jpg
subclass: 'post'
author: antoinevadot
excerpt: Que pensent les Français·es de la protection de leurs données personnelles ? Ont-ils·elles confiance dans les sites de leur quotidien ? Que font-ils·elles pour se protéger ?
---

Que pensent les Français·es de la protection de leurs données personnelles ?
Ont-ils·elles confiance dans les sites de leur quotidien ?
Que font-ils·elles pour se protéger ?

Nous avons voulu l'avis des Français·es.

### Sondage Misakey x OpinionWay

Nous avons donc contacté [Opinion Way](https://www.opinion-way.com/), un institut de sondage français, pour obtenir les réponses à ces questions.

Vous pouvez retrouver les résultats de l'étude [ici](https://static.misakey.com/docs/gdpr-study/SondageOpinionWay-Etude.pdf).

### Résultats du sondage

L'étude s'est montrée révélatrice d'une grande frustration des Français·es :
* Les Français·es sont conscient·es de la menace que représente la collecte de leurs données
personnelles par les entreprises et les administrations sur le respect de leur vie privée et leurs
libertés individuelles. La possibilité de naviguer sur le web en toute liberté sans laisser de traces
ne leur apparaît pas possible.
* Ils·Elles se montrent démuni·es face à cette situation : considérant que les entreprises ne font pas de la
protection de leurs données personnelles une priorité, ils·elles observent que ces dernières collectent
des informations sans leur consentement et sans leur laisser le choix.
* Dans ce contexte où ils·elles ont le sentiment de subir cette situation, ils·elles ont tendance à accepter la
politique de confidentialité des sites sans les lire. Toutefois, ils·elles n’hésitent pas à boycotter les
entreprises qui sont connues pour ne pas respecter la vie privée de leurs clients.

La synthèse des résultats chiffrés est disponible [ici](https://static.misakey.com/docs/gdpr-study/SondageOpinionWay-SyntheseEtude.pdf).

## Que faire face à ce constat ?

On va développer fin janvier une plateforme en ligne pour aider les Français·es à reprendre le contrôle
sur leurs données personnelles :

Il suffira de s’inscrire pour obtenir le tableau le bord de sa vie numérique avec :
- la liste des sites que vous utilisez au quotidien
- des informations sur les sites (contacts DPO, politiques de confidentialité, liste des cookies…)
- la possibilité d'exercer ses droits en un clic (portabilité, suppression, mise à jour_)


-------
_Pour en savoir plus, notre [communiqué de presse](https://static.misakey.com/docs/gdpr-study/SondageOpinionWay-CommuniquePresse.pdf) et notre [dossier de presse](https://static.misakey.com/docs/gdpr-study/SondageOpinionWay-DossierPresse.pdf) sur le sondage OpinionWay._

_Pour en savoir plus, notre [lettre de mission](https://static.misakey.com/docs/gdpr-study/LettreMissionMisakey.pdf).
