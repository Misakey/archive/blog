---
layout: post
current: post
navigation: True
title: "Étude : 6 mois après le RGPD, où en est-on ?"
date: 2018-12-12 12:01:00
tags: data-privacy
class: post-template
cover: assets/images/posts/2018-12-04-6-mois-apres-rgpd-ou-en-est-on.jpg
subclass: 'post'
author: antoinevadot
excerpt: Six mois après la mise en place du​ ​Règlement Général sur la Protection des Données (RGPD)​, on a dressé un état des lieux du respect des droits d’accès aux données personnelles en France.
---

Six mois après la mise en place du​ Règlement Général sur la Protection des Données (RGPD)​, on a
dressé un état des lieux du respect des droits d’accès aux données personnelles en France.

Mis en place en mai 2018 dans toute l’Union européenne, le RGPD apporte
aux citoyen·nes une meilleure protection de leurs données personnelles. Chacun·e
peut par exemple demander l’accès à ses informations personnelles,
demander la modification, ou demander la suppression d'une de ces données.

## Peu de réponses des entreprises

C’est en faisant le test auprès d’une centaine d’entreprises françaises que nous nous sommes
aperçus des difficultés à obtenir nos informations. Nous nous sommes alors lancés dans une
étude à grande échelle.

Nous avons mis le RGPD à l’épreuve de la pratique
en contactant plus de 400 000 sites pour leur demander s’ils détenaient des
données associées à notre adresse email. L’objectif était d'obtenir des statistiques à grande échelle
au sujet des embûches auxquelles les Français·es font face.

### Mise en place de l'étude

Nous avons automatisé le processus recommandé par la CNIL pour demander des informations à un site
sur ses données personnelles. Nous avons pu contacter 439 164 sites.
Les étapes qu'un·e citoyen·ne doit suivre pour en savoir plus sur l'usage de ses données personnelles :
1. Trouver l'email du responsable des données personnelles du site dans les documents légaux.
2. Rédiger et envoyer un courriel de demande d'accès.
3. Échanger avec le responsable de la protection des données pour confirmer son identité.
4. Réceptionner le résultat de la demande.

Vous pouvez retrouver la méthodologie complète [ici](https://static.misakey.com/docs/gdpr-study/EtudeMisakey-Methodologie.pdf).

### Résultats de l'étude

{% include image.html url="https://static.misakey.com/docs/gdpr-study/EtudeMisakey-Infographie.png" description="Résultats de l'étude en infographie" %}

Vous pouvez télécharger l'infographie [ici](https://static.misakey.com/docs/gdpr-study/EtudeMisakey-Infographie.pdf).

## Que faire face à ce constat ?

On va développer fin janvier une plateforme en ligne pour aider les Français·es à reprendre le contrôle
sur leurs données personnelles :

Il suffira de s’inscrire pour obtenir le tableau le bord de sa vie numérique avec :
- la liste des sites que vous utilisez au quotidien
- des informations sur les sites (contacts DPO, politiques de confidentialité, liste des cookies...)
- la possibilité d'exercer ses droits en un clic (portabilité, suppression, mise à jour...)

-------
_Pour en savoir plus, notre [communiqué de presse](https://static.misakey.com/docs/gdpr-study/EtudeMisakey-CommuniquePresse.pdf) et notre [dossier de presse](https://static.misakey.com/docs/gdpr-study/EtudeMisakey-DossierPresse.pdf) sur notre étude interne._

_Pour en savoir plus, notre [lettre de mission](https://static.misakey.com/docs/gdpr-study/LettreMissionMisakey.pdf).
