---
layout: post
current: post
navigation: True
title: Plus de sécurité et de simplicité sur Internet avec un gestionnaire de mot de passe
tags: securite
date: 2020-06-16 10:00:00
class: post-template
subclass: 'post'
cover: assets/images/posts/gestionnaire-mots-de-passe/cover.png
author: cedricvanrompay
excerpt: |
  Pourquoi toute l'équipe de Misakey vous conseille d'utiliser
  un gestionnaire de mots de passe pour sécuriser et simplifier
  votre vie sur Internet
---

Vous aussi vous vous posez des questions sur la façon dont vous gérez vos mots de passe&nbsp;? La vôtre n'est pas super pratique, mais même avec ça vous n'êtes pas sûr de savoir si elle est sécurisée ou pas&nbsp;?

Eh bien figurez-vous qu'il existe des applications pour ça&nbsp;! Un *gestionnaire de mots de passe* est une application qui vous permet d'enregistrer les mots de passe de chacun de vos comptes. Vos mots de passe sont protégés contre le vol et sont accessibles depuis n'importe lequel de vos appareils (téléphone, ordinateur, tablette&nbsp;…). Plus de risques d'oublier quel mot de passe vous aviez utilisé pour quel compte. Et surtout vous allez enfin pouvoir avoir des mots de passe plus variés, ce qui, beaucoup l'ignorent, est presque plus important que d'avoir des mots de passe suffisamment difficile à deviner&nbsp;!

{% include image.html url="assets/images/posts/gestionnaire-mots-de-passe/home.png" description="L'écran principal de Bitwarden" %}

Dans cet article on vous explique pourquoi installer un gestionnaire de mots de passe est une des choses les plus efficaces que vous pouvez faire pour vous rendre la vie plus simple et être plus en sécurité sur Internet, et pourquoi toutes les autres «&nbsp;astuces&nbsp;» sont des mauvaises idées. Puis on vous en recommande un ─&nbsp;gratuit&nbsp;─ en particulier, Bitwarden, et on vous explique pourquoi c'est celui qu'on utilise et pourquoi on vous le recommande.

## Vos mots de passe sont la clé de votre sécurité sur Internet

La mission de Misakey est de permettre aux entreprises de proposer une expérience utilisateur selon nos standards français de vie privée. Cela passe par le développement d’une application du quotidien pour unifier les comptes utilisateurs. Elle permet aux individus de recevoir des données standardisées, fichiers et messages en toute confidentialité, ainsi que de synchroniser les données reçues et les informations de profil entre leurs applications (De la portabilité RGPD aux Standards du web 2021). Elle fonctionne via un compte portable qui se met en place par API en moins de 24h.

Pour assurer la sécurité de vos données, Misakey utilise le chiffrement de bout en bout. Les données qui transitent par les serveurs de Misakey ne sont lisibles que par vous et les personnes à qui vous avez explicitement donné accès. Pour faire fonctionner cette technologie, votre mot de passe protège des clés secrètes auxquelles vous seul avez accès.

Les bonnes pratiques présentées dans cet article vont vous éviter beaucoup de problèmes de mots de passe piratés ou perdus, que vous utilisiez Misakey ou non. Et ça nous fait plaisir 😉.

## Pourquoi utiliser un gestionnaire de mots de passe est la seule bonne stratégie

Certains ont un seul mot de passe pour tous leurs comptes. C'est très dangereux, parce qu’il suffit que ce mot de passe soit compromis à un seul endroit pour que tous vos comptes, même les plus importants, soient en danger.

Mais si le mot de passe est vraiment difficile à deviner&nbsp;? Ça ne fait pas une grande différence, parce que chaque site Web voit le mot de passe quand vous l'utilisez pour vous connecter (sauf Misakey, qui utilise une technologie particulière pour cela). Pire, le mot de passe est enregistré dans leur serveur, sans quoi le site ne pourrait pas savoir si vous lui donnez le bon mot de passe ou pas quand vous vous connectez. Il suffit qu’un seul des sites se fasse pirater, ou qu'il soit un site malveillant dès le début, pour que des criminel·le·s aient accès à votre mot de passe. Alors tout d'un coup, ces criminel·le·s ont potentiellement accès à tous les autres comptes où vous utilisez ce même mot de passe&nbsp;! C’est d’ailleurs la première chose que font les pirates quand ils ont accès à un mot de passe&nbsp;: ils·elles l’essaient (avec votre identifiant) sur un maximum d’autres sites.

Peu importe, donc, que votre mot de passe soit très difficile à deviner, si c'est pour l'utiliser à la fois sur des comptes qui sont très importants pour vous (votre adresse email principale, par exemple) mais aussi pour n'importe quel site suspicieux sur lequel vous avez un jour eu besoin de créer un compte. Même si un·e pirate ne peut pas forcément hacker vos comptes importants directement, il·elle peut toujours récupérer votre mot de passe via un de ces sites à la sécurité douteuse, et ensuite l'utiliser pour avoir accès aux comptes qui ont plus de valeur.

Est-ce que ça suffit alors d'avoir un mot de passe pour les comptes les plus importants, et un autre pour ceux qui le sont moins&nbsp;? Niveau sécurité en effet, c'est un peu mieux. Mais en pratique on s'y perd très vite. Certains sites vont refuser le mot de passe que vous voulez utiliser parce qu'ils ont des exigences bizarres sur la longueur ou les caractères à utiliser pour le mot de passe&nbsp;; parfois un site va vous demander de changer votre mot de passe pour telle ou telle raison. Et très vite, votre règle de «&nbsp;deux mots de passe&nbsp;» va se retrouver avec plein d'exceptions. Bonne chance pour vous en souvenir&nbsp;!

Du coup, beaucoup de gens notent leurs mots de passe quelque part, par exemple dans le répertoire de leur téléphone, parce que c'est plus pratique. Mais c'est loin d'être idéal niveau sécurité&nbsp;: si vous ne verrouillez pas votre téléphone, vos mots de passe sont à la portée de tous. Et même si vous le verrouillez, qui n'a jamais dit à quelqu'un d'autre comment déverrouiller son téléphone, par exemple pour qu'il puisse changer la musique ou répondre à un message pendant qu'on conduit&nbsp;?

{% include image.html url="assets/images/posts/gestionnaire-mots-de-passe/add.png" description="Génération d'un nouveau mot de passe dans Bitwarden" %}

Avec un gestionnaire de mot de passe, il n'y a plus qu'un seul mot de passe dont vous devez vous souvenir, c'est celui du gestionnaire. Ce mot de passe unique vous donne accès à tous les mots de passe de tous vos comptes. Cela vous permet d'en utiliser un différent pour chaque compte, ce qui est essentiel pour être en sécurité sur Internet. Aussi, la plupart des gestionnaires de mots de passe incluent d'autres outils pour vous faciliter la vie avec les mots de passe&nbsp;: ils peuvent générer pour vous des mots de passe très sécurisés, et ils vous évitent d'avoir à taper les mots de passe vous-même en vous permettant de les copier-coller. Certains gestionnaires peuvent même saisir les mots de passe à votre place quand ils détectent que vous essayez de vous connecter à un de vos comptes&nbsp;!

## Bitwarden, le gestionnaire qu'on utilise et qu'on vous recommande

Des applications de gestionnaire de mots de passe, il en existe plein. Mais très vite, des informaticien·ne·s se sont dit que c'est un outil dont ils·elles ont besoin et qu'ils·elles seraient capables de créer eux·elles-même. Cela a donné naissance au gestionnaire de mots de passe KeePass, un logiciel dit «&nbsp;libre&nbsp;», c'est-à-dire qu'il n'est la propriété de personne. La plupart des autres gestionnaires de mots de passe en revanche sont la propriété de l'entreprise qui l'a créé, qui peut à tout moment décider de changer tout ce qu'elle veut dans le programme ou vous obliger à payer pour continuer à l'utiliser.

{% include image.html url="assets/images/posts/gestionnaire-mots-de-passe/keepassxc.jpg" description="Capture d'écran de KeePassXC" %}

Malheureusement KeePass, et son «&nbsp;clone&nbsp;» plus moderne KeePassXC, ne sont pas très pratiques à utiliser, sauf peut-être si vous êtes informaticien·ne vous-même. Heureusement, un nouveau logiciel libre de gestionnaire de mot de passe est apparu, qui est beaucoup plus simple d'utilisation, plus simple à vrai dire que beaucoup des gestionnaires commerciaux&nbsp;: Bitwarden. Bitwarden a très vite obtenu la confiance d'une grande partie de la communauté des professionnel·le·s de l'informatique, et tout le monde à Misakey l'utilise. Bitwarden a une version gratuite qui est largement suffisante pour la plupart des utilisateurs·trices, la version payante est principalement pour les professionnel·le·s et pour soutenir financièrement le projet.

{% include image.html url="assets/images/posts/gestionnaire-mots-de-passe/bitwarden-desktop.png" description="Bitwarden en version pour ordinateur" %}

Pour l'installer sur un téléphone ou une tablette Android vous pouvez aller [ici](https://play.google.com/store/apps/details?id=com.x8bit.bitwarden), et pour un iPhone ou iPad vous pouvez aller [là](https://itunes.apple.com/app/bitwarden-free-password-manager/id1137397744). Vous pouvez aussi l'installer sur un ordinateur Windows, Mac ou Linux en allant sur [leur site Web](https://bitwarden.com/#download).

## Conclusion

Évitez-vous des problèmes et installez un gestionnaire de mots de passe, que ce soit Bitwarden ou un autre gestionnaire qu'une personne en qui vous avez confiance vous a recommandé. Après ça vous n'aurez plus qu'un seul mot de passe dont vous devez vous souvenir&nbsp;: celui du gestionnaire.

Et si c'est mieux pour vous, c'est mieux pour nous&nbsp;! Misakey utilise une technologie particulière, le «&nbsp;chiffrement de bout en bout&nbsp;», pour garantir que personne, pas même Misakey, ne puisse lire les données que vous partagez sur Misakey, sauf si vous lui en donnez explicitement l'accès. Malheureusement, cela veut dire que si vous perdez votre mot de passe, Misakey ne peut pas vous aider à récupérer l'accès à vos données. On tient donc beaucoup à ce que vos mots de passe soient en sécurité quelque part pour que votre expérience sur Misakey soit 100% sécurisée et 100% sans problèmes.
