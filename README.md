# Le Blog de Misakey

🇬🇧 English: The blog content is French for now, so this repo is in French too.

🇫🇷 Français : vu que le contenu du Blog est en Français, on parle principalement Français sur ce projet.


Misakey est la solution de comptes utilisateurs open source la plus simple pour protéger la confidentialité des activités de son entreprise, son association, sa famille ou son équipe.


Ce blog est construit avec [jekyll](https://jekyllrb.com/) et une version modifiée du thème [jasper2](https://github.com/jekyller/jasper2).

## Construire Localement

- installer toute la stack `ruby` (`sudo apt install ruby-full`)
- si vous ne voulez pas installer vos *gems* en tant que `root` il y a des instruction dans la doc de Jekyll [ici](https://jekyllrb.com/docs/installation/ubuntu/)
- `gem install bundler jekyll`
- à la racine du projet `blog`:
  - `bundle install`
  - `bundle exec jekyll serve`

### Dépendances potentiellement manquantes

Pour ubuntu :

- gcc : `sudo apt install gcc`
- g++ : `sudo apt install g++`
- dépendances de nokogiri : [guide](https://nokogiri.org/tutorials/installing_nokogiri.html#install-with-included-libraries-recommended)

## Contribuer

- créer un nouveau ticket avec le label ~article
- créer une branche partant de `dev`;
  merci de nommer la branche `article/{n° ticket}-...`
- créer un fichier `titre-de-mon-article.md` dans le dossier `_drafts`,
  qui commence par:

```yaml
---
layout: post
current: post
navigation: True
title: (Titre de l'article)
tags: tag1 tag2
class: post-template
subclass: 'post'
author: firstnamename
excerpt: (Résumé de l'article)
---
```

- merci d'utiliser des outils de corrections orthographique et grammaticale comme [Grammalecte](https://grammalecte.net/#download) avant d'envoyer l'article pour relecture
- quand l' article semble prêt, assigner tout le monde sur la merge request
- quand l'article a été accepté par tout le monde, incorporer dans la branche `dev`

Un article n'est publié que quand il est incorporé dans la branche `master`.
Quand on décide de publier un article:

- on ajoute une ligne `date: 20xx-xx-xx xx:xx:xx` dans l'en-tête
- on déplace le fichier dans `_posts/` en mettant la date de publication au début du fichier
  (qui devient `YYYY-MM-DD-titre-de-mon-article.md`)
- on fait avancer `master` au niveau de `dev`

Noter que `master` contient les brouillons mais le blog officiel est construit sans l'option `--drafts`.
C'est ce qui fait qu'ils n'apparaissent pas dans la version « officielle » du blog.

## License

Le code et le contenu du blog sont sous la license GPLv3. 
Si un sous-dossier contient une autre license, c'est cette license qui s'applique.

## Gestion du code Source

Misakey utilises GitLab pour développer ses logiciels. Nos repos Github ne sont que des mirroirs.
Si vous voulez contribuer, merci de nous forker sur gitlab.com
(pas besoin d'inscription, vous pouvez vous connecter avec votre compte Github)
