---
layout: post
current: post
navigation: True
title: Quels risques j'encours au quotidien avec le vol de données numériques ?
tags: sécurité RGPD
class: post-template
subclass: 'post'
cover: assets/images/posts/vol-donnees-numeriques/cover.png
author: cedricdupuis
excerpt: Focus sur le vol de données numériques. Nous vous expliquons pourquoi il est urgent et nécessaire que chacun reprenne le contrôle sur ses données personnelles
---

Dans cet article, vous apprendrez pourquoi le vol de données numériques est lucratif.<br />
Vous saurez quelles données personnelles sont volées, et lesquelles sont généralement déjà disponibles sur internet.<br />
Vous connaîtrez quelques-unes des méthodes utilisées par les pirates.<br />

_Nous espérons que, fort de ces nouveaux savoirs, vous prendrez conscience qu'il est urgent et nécessaire que chacun reprenne le contrôle sur ses données personnelles sensibles._

---

Avant de parler de vol de données personnelles numériques, précisons un peu le sujet.

D'après la CNIL&nbsp;:
> Une donnée personnelle est toute information se rapportant à une personne physique identifiée ou identifiable.

Certaines sont _directement nominatives_ —&nbsp;elles permettent de vous identifier immédiatement&nbsp;— par exemple&nbsp;:<br/>
nom, prénom, photo du visage, numéro de sécurité sociale, numéro d'une pièce d'identité, …

D'autres sont _indirectement nominatives_&nbsp;:<br/>
numéro de téléphone, adresse postale, photos, vidéos, mail, date de naissance, adresse IP, groupe sanguin, …

On parle de _traitement de données personnelles_ lorsque les données sont manipulées par un tiers.

Dans votre utilisation quotidienne d'internet, vous fournissez des informations personnelles à une grande diversité d'applications et de services. Ces données sont ensuite traitées par les entreprises aux commandes de ces services, pour, par exemple, personnaliser votre expérience.

Les entreprises sont tenues responsables par la loi vis-à-vis de la protection de vos données personnelles.

_À venir un article dédié pour approfondir le sujet_

## Qu'est-ce que le vol de données personnelles numériques&nbsp;?

C'est la récupération illégale par un individu d'informations relatives à votre identité numérique.<br/>
Il n'y a pas besoin de prouver la réutilisation mal intentionnée de la donnée, seulement le vol, qui est en lui-même illicite.

> Rappel&nbsp;: les applications auxquelles vous fournissez des données personnelles en sont responsables.

En pratique, malgré des règles très strictes, les applications se font régulièrement voler des données personnelles d'utilisateurs. On parle alors de _fuite de données_.

Diverses illustrations dans [cet article du journal Le Point](https://www.lepoint.fr/high-tech-internet/securite-les-principaux-vols-de-donnees-personnelles-15-05-2018-2218483_47.php).

> **Attention**&nbsp;: il n'y a pas de vol de données lorsque celles-ci ont été délibérément rendues accessibles.
> - Sur les réseaux sociaux, lorsqu'on définit les règles d'accès et de visibilité concernant le contenu publié.<br />
> - Lors de contributions publiques à des sites (Articles Wikipédia, forums, journaux en ligne), dont la visibilité, la réutilisation et le partage sont régis par les CGU desdits sites, parfois sous la protection de licences.

## Pourquoi le vol de données numériques prolifère-t-il&nbsp;?

Tout d'abord, il existe de fortes disparités au sein des internautes.<br/>
Internet est un médium à évolution si rapide qu'il est difficile de se tenir à jour quant à son utilisation en sécurité.

Une [étude de l'Insee sur l'année 2019](https://www.insee.fr/fr/statistiques/4241397) montre qu'en moyenne en France, plus d'1 internaute sur 3 manque des compétences numériques de base.

Dans ces conditions, il est facile de devenir la proie d'utilisateurs plus expérimentés et mal intentionnés.

Internet ne cesse de rassembler de plus en plus d'utilisateurs réguliers.

[Médiamétrie](https://www.mediametrie.fr/) révèle dans [un communiqué de presse sur l'année 2019](https://www.mediametrie.fr/fr/lannee-internet-2019) que **92% des foyers français** sont équipés pour accéder à internet, ce qui représente 53 millions d'internautes. L'article évoque une utilisation **quotidienne** d'internet chez ceux-ci.

Rien qu'en France, ce sont plus de 15 millions d'internautes réguliers qui se révèlent être des proies faciles pour les pirates.

Au cours des dernières années, nous avons vécu une large démocratisation des technologies du numérique. Aujourd'hui, tous les secteurs ont leur application numérique.

Les outils d'anonymisation sur le net se sont répandus.<br/>
En première ligne, les VPN, qui permettent à un utilisateur de changer son adresse IP en faisant passer ses données par des serveurs intermédiaires, donc masquer sa vraie localisation, parfois même pour les agences gouvernementales.

> Pour approfondir&nbsp;: L'équipe du LaboFnac propose [un dossier détaillé sur les VPN](https://labo.fnac.com/guide/vpn-protegez-votre-anonymat-sur-internet/)

La communication instantanée est entrée dans une nouvelle ère avec la notion de chat chiffré. Exemple d'applications&nbsp;: Signal, Riot, Telegram, Zoom, Whatsapp.

Enfin, transformation à double tranchant, la dématérialisation des usages apporte son lot de possibilités&nbsp;: applications bancaires, outils de virement dématérialisé, achats en ligne, etc.

…Mais aussi de dangers&nbsp;: accès à un compte bancaire via identifiant & mot de passe, paiements sans vérification d'identité, etc.

**Dans la même mouvance**, le piratage s'est développé, démocratisé et industrialisé.

En premier lieu, les outils du numérique peuvent être facilement détournés à des fins malveillantes&nbsp;:
- S'assurer des communications anonymes, privées et sécurisées, c'est du pain béni pour les délits d'association de malfaiteurs ou de recel de données volées.
- Grâce à un ou plusieurs VPN, un pirate peut minimiser le risque d'être identifié, en complexifiant sa traçabilité.

L'outillage des pirates s'est développé.

Des services de hack utilisables par n'importe qui moyennant paiement sont disponibles sur le dark web ([wikipédia vous explique ce mot](https://fr.wikipedia.org/wiki/Dark_web)).<br/>
Ce nouveau marché se nomme le «Hack As A Service».

Le Ministère de l'Intérieur [signale](https://www.interieur.gouv.fr/Actualites/Communiques/L-etat-de-la-menace-liee-au-numerique-en-2019) depuis 2017 l'augmentation du nombre de marchés secondaires de recel de données personnelles.
Ce communiqué révèle que les «attaques par rançongiciel semblent davantage cibler les grandes entreprises ayant la capacité de payer des rançons très élevées».

Une [étude de l'Insee](https://www.insee.fr/fr/statistiques/4472399) révèle que le tiers des grandes sociétés françaises a vécu un incident de sécurité informatique dans l'année 2018.

Internet est devenu le lieu de prédilection de personnes souhaitant commettre des vols en toute impunité.
De par ses principes fondateurs libertaires, il redistribue les cartes en matière de vol.

## Que font les pirates avec vos données&nbsp;?

Certaines données personnelles permettent d'effectuer des achats —&nbsp;voire des retraits&nbsp;— en ligne.

Par exemple, un code de carte bancaire, un RIB, les identifiants d'une application de paiement en ligne.

Les applications stockant ce type de données sont généralement hautement sécurisées, donc difficiles à pirater.

Une stratégie alternative populaire chez les pirates consiste à se faire passer pour ces services et vous demander de renseigner les données citées.<br/>
De nombreux services recommandent de ne jamais répondre à des sollicitations par mail ou téléphone en leur nom, vous demandant de saisir vos identifiants ou des coordonnées privées. Pour cause&nbsp;: il est relativement facile de falsifier une adresse email ou d'abuser de votre confiance par téléphone. Plus de coffre-fort à déverrouiller&nbsp;!

{% include image.html url="assets/images/posts/vol-donnees-numeriques/securite-phishing.png" description="Extrait d'un mail de la Direction Générale des Finances Publiques (DGFiP)" %}

En récupérant suffisamment de données d'identification, il devient possible pour un pirate d'usurper votre identité en ligne.<br/>
Les informations nécessaires à leur succès vont dépendre de l'entité à berner&nbsp;: une institution précise, un autre individu, par exemple.

Nous décortiquons ci-après le stratagème.

Pour souscrire à des contrats, des emprunts ou des abonnements en votre nom, le pirate aura besoin d'identifiants de vos comptes.
Il devra potentiellement renseigner vos coordonnées téléphoniques ou bancaires, votre adresse.

> Pour pallier ce type de problème, les secteurs concernés (banque, télécom) ont depuis complexifié leurs processus de contractualisation.

Moins grave, mais plus difficile à soupçonner, avec les bons identifiants, un pirate pourrait profiter des services auxquels vous avez déjà souscrit, à votre place.<br />
Un abonnement Netflix, ou même votre couverture santé, par exemple.

Un pirate pourrait aussi se faire passer pour vous auprès de vos connaissances.<br/>
Avec les accès à un compte de messagerie ou sur un réseau social, un pirate a accès à vos contacts.
Les informations suffisant à berner leur confiance pourraient surprendre&nbsp;:
votre adresse mail, votre compte sur un réseau social, votre numéro de téléphone, vos nom, prénom, quelques photos de vous, votre adresse, ainsi qu'une situation —&nbsp;tout à fait fictive&nbsp;— d'urgence, pourraient suffire.

Autre possibilité, répliquer son attaque auprès de vos contacts.
Il pourra toujours profiter du surplus de confiance et de la surprise de vos amis.

{% include image.html url="assets/images/posts/vol-donnees-numeriques/mail-phishing.png" description="Exemple de mail d'arnaque" %}

Crédible&nbsp;? À vous seuls d'en juger.<br/>
[Western Union dénombre plusieurs exemples d'escroqueries basées sur l'usurpation d'identité](https://www.westernunion.com/fr/fr/protection-des-consommateurs/arnaque-types.html)

> Pour approfondir&nbsp;: [les recommandations Que Choisir](https://www.quechoisir.org/conseils-arnaques-sur-internet-detecter-les-e-mails-malveillants-n7185/) pour vous aider à détecter les emails malveillants

Une source de risque moindre pour un pirate est de revendre vos données permettant le profit ou la fraude à des tiers.

Ces tiers sont contactables via le dark web. Certains se concentrent sur la collecte de données volées et le recel, un peu comme des grossistes.<br/>
Des sites cachés, des groupes sur des messageries cryptées, permettent d'entrer en contact avec eux.

Comment fonctionne l'économie du vol de données personnelles&nbsp;?

La valeur de revente de vos données est dépendante de ce qu'il est possible d'en tirer, mais aussi de la difficulté avec laquelle elles sont collectées.

> Pour approfondir&nbsp;: dossier [Zataz, itinéraire de la fuite de données](https://www.zataz.com/itineraire-dune-fuite-de-donnees/)

Dans le cas de données sensibles ou de valeur, le chantage et la rançon sont monnaie courante chez les pirates.<br/>
En menaçant de publier des conversations privées, des photos intimes, des informations compromettantes, un pirate peut vous faire chanter et vous soutirer une contrepartie.

Sur les réseaux sociaux, la frontière est mince entre identité virtuelle et identité réelle.<br/>
- Méfiez-vous des personnes auxquelles vous donnez accès à vos informations personnelles, notamment vos photos.
- Évitez d'envoyer de vous-même des informations intimes à des inconnus, si vous ne pouvez pas vérifier qui se cache derrière leur identité virtuelle.

Comme l'indique cet [article de Zataz](https://www.zataz.com/chantage-par-mail-non-vous-navez-pas-ete-pirate/), de nombreuses menaces ne sont que du bluff.<br/>
Un pirate peut avoir récupéré des informations sur vous grâce aux bases de données de sites piratés. Elles sont souvent partagées en masse sur les réseaux de pirates.

Surtout, **ne paniquez pas**.

Ne téléchargez aucun fichier, ne suivez aucun lien provenant d'un tel message, ne répondez pas. Assurez-vous que ce que vous raconte le pirate est réalisable. En cas de doute, renseignez-vous auprès de sites spécialisés, d'experts ou de vos connaissances.

De plus en plus de pirates utilisent les ransomwares, des logiciels qui sont capables de s'approprier certaines de vos données et de vous en retirer l'accès.<br/>
Ils agissent généralement directement sur votre ordinateur, une fois que vous leur avez donné cet accès, comme vous le feriez avec des logiciels classiques.

{% include image.html url="assets/images/posts/vol-donnees-numeriques/ransomware-wannacry.png" description="Capture d'écran du ransomware WannaCry en pleine rançon, wikipedia.org" %}
---

### En résumé

{% include image.html url="assets/images/posts/vol-donnees-numeriques/resume-vol-donnees.png" description="Le vol de vos données personnelles, en résumé" %}

## Que faire pour assurer la sécurité de mes données&nbsp;?

La démocratisation des outils de piratage a entraîné une hausse de la fréquence des petits piratages (réseaux sociaux, phishing, etc.) ainsi que de leur diversité&nbsp;: de plus en plus de méthodes à disposition des pirates.

En tant qu'utilisateur, nous nous fondons dans la masse des plusieurs milliards d'autres internautes quotidiens.<br/>
Au cours de notre navigation, nous risquons majoritairement de rencontrer des pièges «de masse», c'est-à-dire des pièges automatisés et applicables à plus ou moins n'importe qui.

C'est pourquoi nous sommes généralement ciblés par de petites escroqueries ou tentatives d'abus de confiance, ce via des outils numériques pas totalement maîtrisés.

Avec un peu de bon sens et les basiques de ce qu'il est possible de faire depuis internet, le risque de se faire directement pirater est fortement réduit.

Un mot d'ordre dans nos interactions&nbsp;: ne pas répondre en cas de doute quant à l'identité de notre interlocuteur.

Deuxième point d'attention&nbsp;: les informations que détiennent les applications que nous utilisons et leur perméabilité à la fuite de données.

Il est difficile, voire impossible de vérifier la sécurité d'une application.<br/>
Ce qui reste en le pouvoir de chaque internaute, c'est précisément de choisir ce qui est stocké sur internet.<br/>
Concrètement, moins je stocke d'informations durables sur un service, moins je prends de risques.

Exemple - les moyens de paiement en ligne&nbsp;:

Enregistrer ses informations de paiement sur une application, c'est un gain de temps, mais aussi un risque en cas de piratage de mon compte&nbsp;: j'offre l'utilisation immédiate de mon moyen de paiement sur l'application concernée.

Comment pallier ce type de problème si j'ai envie de profiter de tels services&nbsp;?

- En augmentant la sécurité de mes moyens de connexion (authentification en 2 étapes, confirmation par sms, etc.)
- En supprimant mes données personnelles sur les applications que je n'utilise plus

Mon compte Paypal ne me sert plus&nbsp;?
- Au minimum, je supprime mes accès à mes moyens de paiement
- Je peux supprimer mon compte
- Pour m'assurer de ne laisser aucune trace de mon utilisation de Paypal, je peux effectuer une demande RGPD de suppression de mes données personnelles


## Sources

- CNIL, [Contenu soumis à la licence CC-BY-ND 3.0](https://creativecommons.org/licenses/by-nd/3.0/fr/) (extrait le 15/05/2020)
  - [Données personnelles](http://cnil.fr/fr/definition/donnee-personnelle)
  - [Traitement de données personnelles]https://www.cnil.fr/fr/definition/traitement-de-donnees-personnelles)
- Quora, [How do hackers make money](https://www.quora.com/How-do-hackers-make-money)
- Blog Trezor, [Why do hackers hack, and what happens to your stolen data](https://blog.trezor.io/trezor-data-privacy-series-why-do-hackers-hack-and-what-happens-to-your-stolen-data-60d93bf351a4)
- Zataz, Contenu soumis à la propriété intellectuelle
  - [Itinéraire d'une fuite de données](https://www.zataz.com/itineraire-dune-fuite-de-donnees/)
  - [Chantage par mail](https://www.zataz.com/chantage-par-mail-non-vous-navez-pas-ete-pirate/)
- Wavestone, [Le Hack As A Service (HAAS), un marché florissant](https://www.riskinsight-wavestone.com/2012/09/le-hack-as-a-service-haas-un-marche-florissant/)
- Western Union, [types de fraude courants](https://www.westernunion.com/fr/fr/protection-des-consommateurs/arnaque-types.html)
- Wikipédia, [Contenu soumis à la licence CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.fr) (extrait le 15/05/2020)
  - [Ransomware WannaCry](https://fr.wikipedia.org/wiki/WannaCry)
  - [Identité numérique](https://fr.wikipedia.org/wiki/Identit%C3%A9_num%C3%A9rique)
  - [RGPD](https://fr.wikipedia.org/wiki/R%C3%A8glement_g%C3%A9n%C3%A9ral_sur_la_protection_des_donn%C3%A9es)
  - [Dark Web](https://fr.wikipedia.org/wiki/Dark_web)
- Insee, [Contenu partageable](https://www.insee.fr/fr/information/2381863)
  - [Une personne sur six n’utilise pas internet, plus d’un usager sur trois manque de compétences numériques de base](https://www.insee.fr/fr/statistiques/4241397)
- Médiamétrie, [Contenu partageable sans modification](https://www.mediametrie.fr/fr/mentions-legales)
  - [L’année Internet 2019](https://www.mediametrie.fr/fr/lannee-internet-2019)
- Gouvernement, Contenu public
  - [Etat de la menace liée au numérique en 2019](https://www.interieur.gouv.fr/Actualites/Communiques/L-etat-de-la-menace-liee-au-numerique-en-2019)
  - [Campagnes d’arnaques au chantage à la webcam prétendue piratée](https://www.cybermalveillance.gouv.fr/tous-nos-contenus/actualites/campagnes-darnaques-au-chantage-a-la-webcam-pretendue-piratee)
- LaboFnac, [VPN, le meilleur moyen de protéger votre anonymat](https://labo.fnac.com/guide/vpn-protegez-votre-anonymat-sur-internet/)
- Le Point, Contenu journalistique [Sécurité : les principaux vols de données personnelles](https://www.lepoint.fr/high-tech-internet/securite-les-principaux-vols-de-donnees-personnelles-15-05-2018-2218483_47.php)
- Que Choisir, Contenu journalistique [Arnaques sur Internet, détecter les emails malveillants](https://www.quechoisir.org/conseils-arnaques-sur-internet-detecter-les-e-mails-malveillants-n7185/)
