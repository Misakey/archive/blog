---
layout: post
current: post
navigation: True
title: Comment bien gérer ses mots de passe
tags: sécurité
class: post-template
subclass: 'post'
author: cedricvanrompay
excerpt: TODO
---

Tout le monde utilise des mots de passe, et tout le monde sait que c'est important de ne pas se les faire voler. Mais beaucoup de gens gèrent leurs mots de passe d'une façon qui est peu sécurisée, ou alors pas pratique du tout, ou souvent ... les deux à la fois.

Dans cet article on vous explique comment vous en sortir avec la gestion des mots de passe, même si vous n'êtes pas un crack en informatique.

À vrai dire la réponse est toute simple et tient en quelques mots : utilisez un gestionnaire de mots de passe. C'est un programme conçu justement pour vous faciliter la vie avec vos mots de passe, c'est facile à utiliser, et c'est mieux que n'importe quelle « astuce » à laquelle vous pourriez penser. Il en existe plusieurs qui sont gratuits et très bien fait donc vous allez forcément trouver celui qui vous convient. Et surtout : on va tout vous dire sur leur utilisation !


## Gestionnaire de mot de passe : en quoi ça consiste et comment l'utiliser

Un gestionnaire de mot de passe est un programme (ou une « application » quand vous l'installez sur votre téléphone) dans lequel vous pouvez noter vos mots de passe pour vous en souvenir. Comme les mots de passe sont quelque chose de très sensible, l'application va vous demander de taper un « mot de passe maître » pour avoir accès aux mots de passe stockés. Donc il y a toujours un mot de passe au moins dont vous devez vous souvenir, c'est ce « mot de passe maître » qui donne accès à tous les autres. Mais c'est nettement plus pratique que ce souvenir de « quel mot passe j'ai utilisé pour quel compte ».

En plus de cela, la plupart des gestionnaires de mots de passe vous permettent d'avoir accès à tous vos mots de passe depuis n'importe lequel de vos appareils, ordinateurs, téléphones ou tablettes. Pour les plus motivés, vous pouvez configurer votre gestionnaire pour qu'il puisse remplir les mots de passe lui-même quand il détecte un site internet pour lequel il connaît votre mot de passe.

Il y a plusieurs bons gestionnaires de mots de passe qui sont gratuits. Celui qu'on vous recommande en particulier est Bitwarden. Plus loin dans l'article (TODO lien) on vous en présente d'autres, on vous explique les différences et on vous explique nos critères pour décider si un gestionnaire de mots de passe est digne de confiance. En revanche tous nos exemples utiliseront Bitwarden (les captures d’écran sont faites avec la version Android, donc celle des téléphones qui ne sont pas des iPhones).

![Écran de création de compte dans Bitwarden](/assets/images/posts/mots-de-passe/création-compte.png)

On vous conseille de commencer par l'installer sur votre téléphone, parce que c'est le seul appareil qui vous suit partout. Si vous avez un iPhone vous pouvez l'installer [ici](https://itunes.apple.com/app/bitwarden-free-password-manager/id1137397744), et si vous avez un téléphone Android vous pouvez l'installer [là](https://play.google.com/store/apps/details?id=com.x8bit.bitwarden).

Vous allez devoir créer un compte, le valider avec un email de confirmation, puis vous connecter dans l'application avec le compte que vous venez de créer. Le mot de passe que vous allez utiliser pour votre compte est le fameux « mot de passe maître », alors autant en choisir un bien sécurisé. On vous donne des techniques pour créer un mot de passe sécurisé et pas trop dur à se souvenir [plus loin dans cet article](#comment-choisir-un-mot-de-passe-fort-et-dont-vous-vous-souviendrez). Au pire, vous pourrez changer votre mot de passe maître plus tard quand vous vous serez décidé sur un mot de passe suffisamment fort.

![Ajout d'un mot de passe](/assets/images/posts/mots-de-passe/ajout-mot-de-passe.png)

![Utilisation d'un mot de passe enregistré](/assets/images/posts/mots-de-passe/utilisation-mot-de-passe.png)

Ensuite, c'est assez évident. Vous avez un bouton « + » pour ajouter un nouveau mot de passe. Dans le champ « Nom » vous lui donnez un nom (par exemple « Misakey » si c'est pour votre compte Misakey 😉), dans le champ « Nom d'utilisateur » vous mettez votre identifiant (typiquement l'adresse email que vous utilisez pour vous connecter à ce compte) et dans « Mot de passe » vous mettez votre mot de passe. Si vous voulez rajouter des notes à propos de ce mot de passe ou du compte associé il y a un champ « notes » tout en bas. Puis vous appuyez sur « Enregistrer » en haut, et c'est fait ! Vous devriez revenir à l'écran d'accueil et le mot de passe enregistré devrait apparaître dans la liste. Si vous appuyez sur le mot de passe que vous venez de créer, vous arrivez sur sa fiche. Vous pouvez voir le nom d'utilisateur et pour voir le mot de passe vous devez toucher l'icône en forme d’œil. Il y a aussi une icône pour copier le mot de passe directement, pour aller le coller là où il faut sans se fatiguer à le taper. Vous avez aussi un bouton en forme de crayon pour le modifier.

Et c'est pas plus compliqué que ça !


## Pourquoi rien n'est mieux que d'utiliser un gestionnaire de mot de passe

Tout le monde a sa « petite méthode personnelle » pour ses mots de passe. C'est rarement une bonne idée.

Certains ont un seul mot de passe pour tous leurs comptes. C'est très dangereux, parce qu’il suffit que ce mot de passe soit compromis à un seul endroit pour que tous vos comptes, même les plus importants, soient en danger.

Mais si le mot de passe est vraiment difficile à deviner ? Ça ne fait pas une grande différence, parce que chaque site sur lequel vous utilisez le mot de passe voit le mot de passe quand vous le saisissez. Pire, le mot de passe est enregistré dans leur serveur, sans quoi le site ne pourrait pas savoir si vous lui donnez le bon mot de passe ou pas quand vous vous connectez. Il suffit qu’un seul des sites se fasse pirater, ou soit un site malveillant dès le début, pour que des criminels aient accès à votre mot de passe, ce qui leur donne accès à tous vos comptes sur tous les autres sites ! C’est d’ailleurs la première chose que font les pirates quand ils ont accès à un mot de passe : ils l’essaient (avec votre identifiant) sur un maximum d’autres sites.

On comprend donc pourquoi c'est dangereux d'utiliser le même mot de passe pour son adresse email principale (un des comptes les plus important que vous ayez) que pour n'importe quel site suspicieux sur lequel vous avez un jour eu besoin de créer un compte. Même si un pirate ne peut pas forcément hacker votre fournisseur d'email directement, il peut toujours récupérer votre mot de passe via un de ces sites à la sécurité douteuse, ce qui lui donne accès à vos emails. C'est pour ça qu'il est souvent recommandé d'utiliser un mot de passe séparé pour vos comptes les plus importants.

Alors est-ce que ça fonctionne d'avoir deux ou trois mots de passe au lieu d'un seul, avec un pour les comptes très importants, un pour ceux qui sont « moyennement importants », et un pour les comptes dont on se fiche ? Pas vraiment. Niveau sécurité c'est vrai que c'est un peu mieux que de n'avoir qu'un seul mot de passe. Mais en pratique on s'y perd très vite. Certains sites vont refuser le mot de passe que vous voulez utiliser parce qu'ils ont des exigences bizarres sur la longueur ou les caractères à utiliser pour le mot de passe. Du coup vous devez soit déroger à la règle et utiliser un autre de vos mots de passe, soit inventer un nouveau mot de passe et vous en souvenir. Parfois un site va vous demander de changer votre mot de passe pour telle ou telle raison. C'est très rapidement le bazar, et la seule chose qui va vous sortir de là, c'est un gestionnaire de mot de passe.

On voit aussi certaines personnes utiliser le répertoire de leur téléphone pour y noter leur différents mots de passe. C'est presque aussi pratique qu'un gestionnaire de mot de passe, mais niveau sécurité c'est plus que limite. Si vous ne verrouillez pas votre téléphone, la première personne qui vous le vole ou qui vous « l'emprunte » discrètement a accès à tous vos secrets. Et même si vous verrouillez votre téléphone, qui n'a jamais donné son code ou son schéma de verrouillage à quelqu'un, par exemple pour qu'il puisse changer la musique ou répondre à un message quand vous conduisez ? Et non, vous n'êtes pas les seuls à mettre des mots de passe dans votre répertoire, donc oui, des gens mal intentionnés vont tout de suite penser à regarder dedans.

La lutte est vaine. Tout résistance est futile. Venez rejoindre le club des gens cools qui utilisent un gestionnaire de mots passe.


## Quelques trucs et astuces


### Comment choisir un mot de passe fort et dont vous vous souviendrez

On vous l'a dit, vous allez tout de même devoir vous souvenir de votre « mot de passe maître », celui avec lequel vous ouvrez votre gestionnaire de mots de passe. Autant s'assurer qu'il est suffisamment fort. Les autres mots de passe, vous feriez mieux de laisser le gestionnaire de mot de passe les générer (on vous explique plus loin comment faire).

Il y a plusieurs techniques efficaces qui produisent des bons mots de passe. En voici une qu'on vous recommande. Elle a été popularisée par un blog dessiné très populaire dans l'informatique, [XKCD](https://xkcd.com/936/). Elle est connue dans le milieu sous le nom de « horse battery staple » (à mes souhaits). Elle consiste à prendre simplement quatre mots communs et de les mettre bout-à-bout. Par exemple : « vélo », « pin », « dent » et « bobo » ce qui donne comme mot de passe vélopindentbobo. XKCD recommande de choisir les mots vraiment au hasard, puis d'inventer une histoire autour de ces mots pour vous en rappeler. C'est sans doute plus pratique d'utiliser un de vos souvenirs, quelque chose de personnel mais mémorable. Pour l'exemple précédent, j'ai utilisé un de mes souvenirs: je suis cassé une dent en tombant de vélo dans une pinède quand j'étais petit. Inoubliable, mais impossible à deviner. Attention, évitez les mots où vous êtes susceptibles de vous tromper dans l'orthographe.

Les mots de patois, d'argot ou autres mots peu communs mais qui pourraient avoir une signification spéciale pour vous sont un bon moyen de renforcer le mot de passe, tant que ça ne vous empêche pas de vous en souvenir. Vous pouvez ajouter un « style », comme de mettre une majuscule à chaque mot ou de choisir un caractère spécial (`$`, `!`, virgule, espace ...) entre chaque mot. Attention, appliquez le même style à chaque mot ou vous allez vous y perdre vous-même. Pas de `Vélo$pin,Dent!Bobo`, préférez `vélo;pin;dent;bobo`.

En revanche, on vous déconseille de ne prendre que trois mots. La sécurité d'un mot de passe à trois mots n'est pas de trois quart la sécurité d'un mot de passe à quatre mots : elle est plusieurs milliers de fois moins forte.

Ça peut vous paraître beaucoup de travail et d'imagination pour juste un mot de passe, mais souvenez-vous : avec un gestionnaire de mot de passe, vous n'aurez plus que celui-ci dont vous devez vous souvenir.

Cette technique n'est pas celle qui donne les mots de passe les plus forts, mais elle fournit tout de même des mots passe relativement forts, et faciles à retenir. Une méthode plus forte mais un peu moins pratique consiste à choisir une phrase et à construire le mot de passe avec la première lettre de chaque mot et la ponctuation (vous devriez utiliser au moins 10 caractères).


### Pas besoin de mettre tout vos mots de passe dans le gestionnaire du premier coup

Vous avez sans doute beaucoup de comptes différents, donc ça serait beaucoup de travail de tous les mettre dans votre gestionnaire dès le début.

Ce qu'on vous conseille, c'est de n'enregistrer que les mots de passe des comptes les plus sensibles dans un premier temps. Au passage ce serait l'occasion de changer les mots de passe de ces comptes.

Pour les autres comptes, vous pouvez faire la chose suivante: quand vous devez vous y connecter, vous commencez par regarder si le mot de passe est dans votre gestionnaire. Si oui, tant mieux. Sinon, vous retrouvez votre mot passe avec la méthode que vous utilisiez avant (répertoire, mot de passe unique...) et quand vous l'avez retrouvé vous en profitez pour l'ajouter dans votre gestionnaire (et le changer, potentiellement).


### Laissez votre gestionnaire générer les mots de passe

La plupart des gestionnaires de mots de passe ont un outil pour générer des mots de passe très forts. Souvent ça donne quelque chose presque impossible à se souvenir, mais peu importe vu que le gestionnaire s'en souvient pour vous.

Les outils de génération des mots de passe ont souvent un certain nombre d'options. Voici ce qu'on vous conseille:
* longueur: au moins 10.
* caractères: lettres, majuscules et caractères spéciaux
* activez « éviter les caractères ambigus » pour exclure les caractères qui se ressemblent trop (« I » et « l », « 0 » et « O » ...)

![Génération automatique de mots de passe](/assets/images/posts/mots-de-passe/génération.png)

### Utilisez le copier-coller pour ne pas avoir à taper les mots de passe

Même avec les options indiquées plus haut, les mots de passe ne vont pas être très pratiques à recopier quand vous en aurez besoin. Alors pensez au copier-coller ! La plupart des gestionnaires de mots de passe vous permettent de copier le mot de passe pour pouvoir le coller au bon endroit.

Du coup, pensez aussi à installer votre gestionnaire de mot de passe sur votre ordinateur, pour pouvoir là aussi copier-coller les mots de passe. Pour installer Bitwarden sur votre ordinateur, voir la section « Desktop » de la page d’accueil de leur site web.


### Faites-vous aider

Bitwarden et les autres gestionnaires de mots de passe on d’autres fonctionnalités qui peuvent vous être très utile mais qui sont un peu plus difficiles à mettre en place. Par exemple, l’outil de remplissage automatique : votre téléphone peut détecter quand une application ou un site internet vous demande un mot de passe, et faire apparaître un bouton qui va laisser le gestionnaire de mot de passe le remplir pour vous. Si le gestionnaire a un doute il va vous faire chercher et confirmer le mot de passe à utiliser.

Vous pouvez essayer d’activer cette fonctionnalité vous-même. Par exemple dans Bitwarden sous Android il faut aller dans « paramètres », puis il y a une section « remplissage automatique » dans laquelle vous pouvez activer les services de remplissage automatique.

Si ça vous paraît trop compliqué, vous pouvez demander à quelqu’un dans votre entourage qui est plus à l’aise avec l’informatique que vous. Si vous n’avez pas de proche qui puisse le faire pour vous, vous pouvez vous tourner vers des associations dédiées à « l’informatique libre », qui tiennent souvent des ateliers d’entraide ou organisent des conférences.


## Comment choisir son gestionnaire de mot passe, et comment lui faire confiance ?

On vous a recommandé Bitwarden parce qu'il est gratuit, qu'on trouve qu'il est pratique, et que le projet et l’équipe qui le porte a notre confiance et celle d’une large communauté d’informaticiens.

Il existe d'autres gestionnaires de mots de passe, beaucoup d'autres à vrai dire. Un des avantages de Bitwarden est qu'il est « open-source », c'est à dire que son code informatique est exposé à la vue de tous. Cela permet à de nombreux développeurs intéressés par le projet de surveiller le code source pour s'assurer de l'absence de faille de sécurité ou de morceaux de code malveillant. À vrai dire il est mieux que juste « open-source »: il est distribué « sous licence libre », ce qui veut dire que si les développeurs originaux perdent la confiance de la communauté ou perdent l'envie ou les moyens de travailler dessus, une autre équipe peut se former, cloner le programme et continuer de le maintenir.

Ce genre de garantie est très important pour nous et cela suffit, avec la confiance que lui accorde une large communauté dans le milieu de l'informatique, à classer Bitwarden devant les solutions qui ne sont pas sous licence libre comme Dashlane, 1Password, EnPass, LastPass etc …

Il y a quelques autres gestionnaires de mots de passe qui sont sous licence libre, principalement KeePassXC et son ancêtre KeePass, mais ils nous paraissent moins pratiques.
Si vous avez des doutes sur tel ou tel gestionnaire de mot de passe, essayez de trouver quelqu'un dans votre entourage qui travaille dans l'informatique et demandez-lui son avis.

En revanche on vous déconseille de juste chercher « meilleur gestionnaire de mots de passe » dans un moteur de recherche : la plupart des résultats que vous allez trouver se rapproche plus de la publicité déguisée que de véritables articles. Les auteurs gagnent de l’argent si vous allez visiter le site web des gestionnaires qu’ils vous recommandent. On vous jure que cet article-ci n’est pas rémunéré, et que ce qu’on vous recommande est ce que nous pensons véritablement être le meilleur pour vous. Mais évidemment c’est encore mieux si vous sollicitez l’avis d’autres personnes qui ont votre confiance.
