---
layout: post
current: post
navigation: True
title: Exemple de Brouillon
tags: test
class: post-template
subclass: 'post'
author: cedricvanrompay
excerpt: Un exemple d'article qui doit toujours rester dans le dossier des brouillons.
---

Voir le fichier `README.md` pour le processus de rédaction et de publication.
