---
layout: post
current: post
navigation: True
title: Le Chiffrement de Bout en Bout
tags: crypto
class: post-template
subclass: 'post'
author: cedricvanrompay
excerpt: Le chiffrement de bout en bout est la technologie qui permet à Misakey de gérer vos données sans jamais être capable de les lire. 
---

Le but de Misakey est de redonner aux citoyens le contrôle de leur vie numérique, et pour cela Misakey propose une plateforme via laquelle les citoyens peuvent récupérer et gérer les données personnelles que les sites ont sur eux.

Mais si Misakey était capable de lire les données personnelles qui sont gérées via la plateforme, le résultat ne serait qu'une entreprise de plus à laquelle le citoyen est censé « faire confiance » quant à la gestion de sa vie numérique.

La solution, c'est le *chiffrement de bout en bout*, une technologie qui est en train de changer profondément la façon dont on communique sur Internet.

Dans cet article, qui cherche à être compréhensible pour tous, on vous donne un petit aperçu du chiffrement de bout en bout et de sa mise en place chez Misakey.

## Qu'est-ce que le chiffrement de bout en bout ?

Le protocole HTTPS est apparu dans les années 2000 pour permettre d'échanger des informations de façon sécurisée sur le Web. Grâce à HTTPS, personne ne peut lire le contenu échangé entre votre ordinateur (ou téléphone, ou tablette) et le serveur auquel vous êtes connecté·es. HTTPS peut aussi être utilisé entre deux serveurs qui veulent communiquer de façon chiffrée.

Évidemment les serveurs de Misakey ne communiquent *que* via HTTPS avec le monde extérieur : quand un site dépose vos données sur Misakey, et quand vous venez les récupérer sur votre compte Misakey. Aujourd'hui la majorité des communications sur Internet utilise HTTPS (voir [les statistiques de Let's Encrypt][]) et c'est une excellente chose. Méfiez-vous des sites qui utilisent HTTP et non HTTPS !

![couverture du chiffrement de HTTPS](assets/images/posts/https-bout-en-bout.png)

Le problème avec HTTPS, c'est que notre serveur déchiffre la donnée reçue par les sites, avant de la chiffrer de nouveau pour vous l'envoyer quand vous la récupérez sur Misakey. Cela va contre la mission de Misakey qui est de vous laisser choisir qui peut avoir accès à vos données, quand et sous quelles conditions. Misakey ne sert que de relai entre les sites et vous et n'a pas à voir le contenu de vos données. On veut donc que les données soient chiffrées par les sites de telle manière que seulement vous, et pas Misakey, puissiez les déchiffrer.

C'est un problème qui est apparu assez tôt dans les messageries sur Internet, où là aussi le serveur ne sert que de relai, et qui a été appelé « chiffrement de bout en bout ». Pendant longtemps le chiffrement de bout en bout nécessitait des technologies obscures difficiles à utiliser (principalement [PGP][]). Mais récemment de grands progrès ont été faits dans ce domaine, notamment avec la création du protocole « Signal » développé pour l'application de messagerie chiffrée [Signal][]. Le protocole « Signal » est aujourd'hui utilisé par WhatsApp par défaut dans toutes les conversations et dans Facebook Messenger en mode « conversation secrète ». L'application [Telegram][] a aussi développé son propre protocole de chiffrement de bout en bout. 

Il y a aussi le protocole « Matrix » qui fournit du chiffrement de bout en bout. Ce protocole est utilisé par l'application [Riot][] et est conçu pour pouvoir unifier les différents services de messagerie (et [utilisé par le gouvernement français depuis peu][matrix-gouv-fr]). Au départ nous avions pensé utiliser ce protocole pour les échanges de données entre sites et utilisateurs dans Misakey. Cela nous aurait permis d'avoir du chiffrement de bout en bout presque sans effort. Mais Matrix est pensé pour de la messagerie, et ce que nous faisons est légèrement différent. Nous sommes donc obligés de concevoir notre propre protocole, en s'inspirant fortement des protocoles existants.

## Le chiffrement « à clé publique »

Le chiffrement de bout en bout mis en place chez Misakey repose sur ce qu'on appelle le « chiffrement asymétrique » ou « chiffrement à clé publique » : quand un utilisateur crée un compte sur Misakey, son navigateur va lui créer une paire de clés cryptographiques, l'une appelée *clé publique*, et l'autre, *clé privée*. La clé publique permet à n'importe qui de chiffrer des données à destination de cet utilisateur, et la clé privée permet à l'utilisateur de déchiffrer les données qui ont été chiffrées avec sa clé publique.

![illustration du chiffrement a clé publique](../assets/images/posts/public-key.png)

La clé privée doit être protégée contre le vol : quelqu'un qui vous vole votre clé privée risque d'être capable de déchiffrer vos données. La clé publique en revanche peut être rendue publique, comme son nom l'indique.

Ça peut paraître assez surprenant, que quelqu'un avec la clé publique puisse *chiffrer* des messages mais soit incapable de les *déchiffrer*, mais c'est possible, tout comme un cadenas ne permet que de *fermer* quelque chose et c'est la *clé* du cadenas qui permet d'ouvrir. On a trouvé des fonctions mathématiques qui sont faciles à calculer dans un sens (le chiffrement) mais extrêmement difficiles à inverser (trouver les valeurs d'entrée à partir du résultat) sans une information supplémentaire (la clé privée)[^1].

[^1]: L'exemple le plus connu est la multiplication de grands nombres premiers, problème sur lequel se base l'algorithme cryptographique [RSA][]

Le navigateur envoie la clé publique à Misakey qui la met à la disposition des DPO des sites. Les DPO peuvent alors la récupérer pour chiffrer des données à envoyer à cet utilisateur. La clé privée, elle, ne doit *pas* être connue par Misakey, ou alors ce ne serait plus du chiffrement de bout en bout. On vous explique dans la section d'après comment votre clé privée est sécurisée par Misakey.

## Sauvegarde de la clé privée

Une des complications du chiffrement de bout en bout est que si l'utilisateur perd sa clé privée, il perd l'accès à sa donnée, et personne ne peut l'aider. Sur un site Web normal, si vous avez oublié votre mot de passe il suffit de cliquer sur un bouton qui vous envoie un email contenant un lien pour choisir un nouveau mot de passe, et vous avez à nouveau accès à toutes les fonctionnalités. Avec une clé privée que le serveur ne connaît pas, ça ne fonctionne pas comme ça : pas de clé, pas de données.

C'est dommage, mais si c'est le prix à payer pour la défense de votre vie privée, ça en vaut le coup.

Chez Misakey on ne peut pas vous empêcher de perdre votre clé privée, mais on peut vous simplifier la vie : quand vous créez un compte sur Misakey, votre navigateur génère une clé privée puis la stocke sur notre serveur. Évidemment le navigateur la chiffre avant de l'envoyer au serveur, car si notre serveur a la clé de déchiffrement ça n'est plus vraiment du « chiffrement de bout en bout ». Et c'est votre mot de passe qui est utilisé pour chiffrer votre clé privée.

Malheureusement cela veut dire que en perdant votre mot de passe vous perdrez aussi l'accès à vos données : si vous utilisez « j'ai oublié mon mot de passe » sur Misakey, votre navigateur va vous générer une nouvelle paire de clés pour que les sites puissent vous à nouveau vous envoyer des données, mais vous ne pourrez plus déchiffrer les données qu'on vous avait envoyées *avant* d'avoir utilisé « j'ai oublié mon mot de passe ».

Mais alors à quoi cela a-t-il servi de stocker votre clé privée sur nos serveurs ? Cela a servi à ce que vous n'ayez qu'une seule chose à vous rappeler (votre mot de passe) plutôt que deux (mot de passe *et* clé privée, qui en plus est beaucoup trop longue pour être mémorisée).

Évidemment pour éviter d'oublier son mot de passe on vous conseille d'utiliser un gestionnaire de mot de passe (chez Misakey on aime bien [Bitwarden](https://bitwarden.com/) et [KeePassXC][]).

À vrai dire vous pouvez aussi télécharger votre clé privée depuis l'écran de gestion de votre compte, pour pouvoir l'importer si un jour vous oubliez votre mot de passe. Importer une clé privée vous permet de récupérer l'accès aux données qui vous ont été envoyées à l'époque où cette clé privée était la vôtre. Attention cependant à ne pas vous faire voler votre clé privée !

![exporter sa clé privée dans Misakey](/assets/images/posts/export-cle-misakey.png)

## Améliorations à venir

Nous travaillons sur plusieurs fonctionnalités pour vous aider en cas de perte de votre mot de passe :

- dans un premier temps, votre ancienne clé privée serait *archivée* au lieu d'être effacée quand vous utilisez « j'ai oublié mon mot de passe », vous permettant de la récupérer plus tard si vous retrouvez votre mot de passe
- plus tard, vous aurez la possibilité de nommer d'autres utilisateurs Misakey pour qu'ils reçoivent une « part » de votre clé privée si jamais vous venez à la perdre. Le serveur de Misakey recevra lui aussi une « part ». Une part toute seule ne sert à rien, mais en combinant la part de Misakey avec la part d'un utilisateur que vous avez nommé, on retrouve la clé. Il vous suffira donc de contacter un des utilisateurs que vous aviez choisis pour qu'il vous envoie la part de clé qu'il a reçu. La technique utilisée est appelée « [partage cryptographique de secret][wikipedia: partage de secret] ». On vous expliquera ça plus en détails dans un autre article de blog.

Nous étudions aussi divers moyens pour renforcer la sécurité des clés *publiques* que nous hébergeons. Si un pirate prenait le contrôle de nos serveurs il serait incapable de lire vos données grâce au chiffrement de bout en bout, mais il pourrait remplacer votre clé publique par une clé publique dont il connaît la clé privée. Si après cela un site vous envoie des données, le pirate sera cette fois-ci capable de les lire.



[les statistiques de Let's Encrypt]: https://letsencrypt.org/fr/stats/
[PGP]: https://fr.wikipedia.org/wiki/Pretty_Good_Privacy
[Signal]: https://signal.org/fr/
[Telegram]: https://telegram.org/
[Riot]: https://about.riot.im/
[matrix-gouv-fr]: https://www.nextinpact.com/news/106467-a-decouverte-riot-outil-libre-derriere-future-messagerie-letat-francais.htm
[wikipedia: partage de secret]: https://fr.wikipedia.org/wiki/Secret_r%C3%A9parti
[KeePassXC]: https://keepassxc.org/
[Bitwarden]: https://bitwarden.com/
[RSA]: https://fr.wikipedia.org/wiki/Chiffrement_RSA
