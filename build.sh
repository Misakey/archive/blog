#!/bin/bash

if [[ -z "$ENV" ]]; then
  echo "Must provide an environnement var"
  exit 1
fi

if [ $ENV == 'production' ]
then
  cp _config.prod.yml _config.yml
  jekyll build
elif [ $ENV == 'preprod' ]
then
  cp _config.preprod.yml _config.yml
  jekyll build --drafts
else
  echo 'No valid $ENV provided. Could be: production or preprod'
  exit 1
fi
